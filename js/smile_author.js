$(function () {
  // 如果主圖高度比寬度多(直式或正方)，就加上樣式horizontal
  var mainImgWidth = $(".articleTitle .img picture > img").outerWidth();
  var mainImgHeight = $(".articleTitle .img picture > img").outerHeight();
  if (mainImgHeight >= mainImgWidth) {
    $(".articleTitle")
      .addClass("horizontal")
      .css({ "min-height": mainImgHeight });
    var nowHeight = $(".articleTitle .img picture > img").outerHeight() + 30;
    $(".articleTitle").css({ "min-height": nowHeight });
  }
});
